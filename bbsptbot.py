import os
import telebot
import requests
import logging
import docx

from telebot import types
from telebot.util import smart_split
from openai import OpenAI
from pathlib import Path


TOKEN = os.environ['TELEGRAM_API_KEY']
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

logging_file_handler = logging.FileHandler(f'/bbsptbot/logs/{__name__}.log')
logging_file_handler.setLevel(logging.INFO)

screen_handler = logging.StreamHandler()
screen_handler.setLevel(logging.WARNING)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logging_file_handler.setFormatter(formatter)
screen_handler.setFormatter(formatter)

logger.addHandler(logging_file_handler)
logger.addHandler(screen_handler)

# Initialize bot and dispatcher
bot = telebot.TeleBot(TOKEN)
client = OpenAI()


@bot.message_handler(content_types=['photo'])
def get_photo(message):
    markup = types.InlineKeyboardMarkup()
    markup.add(types.InlineKeyboardButton('Перейти на сайт', url='https://iagorod.kz'))
    btn1 = types.InlineKeyboardButton('Удалить фото', callback_data='delete')
    btn2 = types.InlineKeyboardButton('Изменить текст', callback_data='edit')
    markup.row(btn1, btn2)
    bot.reply_to(message, 'Какое красивое фото!', reply_markup=markup)


@bot.callback_query_handler(func=lambda callback: True)
def callback_message(callback):
    if callback.data == 'delete':
        bot.delete_message(callback.message.chat.id, callback.message.message_id - 1)
    elif callback.data == 'edit':
        bot.edit_message_text('Редактировать текст', callback.message.chat.id, callback.message.message_id)


@bot.message_handler(commands=['code'])
def generate_code(message):
    completion = client.chat.completions.create(
        model="gpt-4-1106-preview",
        messages=[
            {"role": "system", "content": "Coding assistant."},
            {"role": "user", "content": "Предметная область: система организации видеоконференций. Перечислить основные функциональные и нефункциональные требования к разрабатываемому программному обеспечению."}
        ]
    )

    bot.send_message(message.chat.id, str(completion.choices[0].message.content))


@bot.message_handler(commands=['image'])
def info(message):
    response = client.images.generate(
        prompt="Создайте изображение диаграммы классов UML для программного продукта - системы организации видеоконференций. Необходимо представить основные классы и их связи в системе, включая классы пользователей, видеоконференций, серверов или другие сущности, которые могут быть важны для организации и управления видеоконференциями. Уделите внимание связям между классами и их атрибутам и методам, отражающим функциональность системы. Подумайте о разделении ответственностей между классами и их взаимодействии для успешной организации видеоконференций. Учтите важность безопасности, масштабируемости и удобства использования при разработке структуры классов. ",
        n=2,
        size="1024x1024"
    )

    bot.send_message(message.chat.id, response.data[0].url)


@bot.message_handler(commands=['help', 'start', ])
def send_welcome(message):
    bot.send_message(message.chat.id, '''
<strong>Привет!</strong> Я телеграм бот-помощник. Отправь мне аудио и я смогу его превратить в текст. 
''', parse_mode='html')


@bot.message_handler(func=lambda message: True, content_types=['audio', ])
def handle_audio(message):
    file_id = message.audio.file_id
    file_info = bot.get_file(file_id)
    logger.info(file_info)
    logger.info(message.audio.title)
    audio_file_object = Path(f'/bbsptbot/files/{message.audio.title}')
    document_file_object = Path(f'/bbsptbot/files/{message.audio.title}').with_suffix('.docx')

    try:
        logger.info(f'Received audio message with ID: {file_id}')
        file_url = bot.get_file_url(file_id)

        logger.info(f'Audio file URL: {file_url}')
        audio_file = requests.get(file_url)
    except Exception as e:
        logger.error(f'Bot rising following error: {e}')

    try:
        with audio_file_object.open('+wb') as f:
            f.write(audio_file.content)
    except Exception as e:
        logger.error(f'Bot rising following error: {e}')

    try:
        with audio_file_object.open('rb+') as mf:
            transcription = client.audio.transcriptions.create(
                model="whisper-1",
                file=mf,
                response_format="text",
            )
    except Exception as e:
        logger.error(f'Bot rising following error: {e}')

    dock = docx.Document()
    dock.add_paragraph(transcription)
    dock.save(document_file_object.absolute())

    try:
        with document_file_object.open('rb+') as docx_file:
            bot.send_document(message.chat.id, docx_file)
    except Exception as e:
        logger.error(f'Bot rising following error: {e}')

    try:
        for n in smart_split(transcription):
            bot.send_message(message.chat.id, n)
    except Exception as e:
        logger.error(f'Bot rising following error: {e}')


@bot.message_handler(func=lambda message: True, content_types=['voice', ])
def handle_voice(message):
    voicd_id = message.voice.file_id
    message.reply_to(message.chat.id, voicd_id)


@bot.message_handler()
def info(message):
    completion = client.chat.completions.create(
        model="gpt-4-1106-preview",
        messages=[
            {
                "role": "user",
                "content": message.text,
            }
        ]
    )

    try:
        for n in smart_split(str(completion.choices[0].message.content)):
            bot.send_message(message.chat.id, n, parse_mode="markdown")
    except Exception as e:
        logger.error(f'Bot rising following error: {e}')


if __name__ == '__main__':
    logger.info("Starting bbspt bot...")

    try:
        bot.infinity_polling()
    except Exception as e:
        logger.error(f'Bot rising following error: {e}')
