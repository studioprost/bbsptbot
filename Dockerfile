FROM python:3.12-slim-bullseye

WORKDIR /BBSPTBot

#RUN apt update && apt upgrade && apt autoremove \
#    && apt install -qy --no-install-recommends python3-poetry\
#    && apt clean \
#    && useradd --create-home bot \
#    && chown -R bot:bot /BBSPTBot \
#    && rm -Rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man

RUN useradd bot && \
    pip install --upgrade pip && \
    pip install poetry

COPY pyproject.toml .

RUN poetry config virtualenvs.create false && \
    poetry install --no-interaction --no-ansi

USER bot

ENV PYTHONUNBUFFERED=1 \
    TELEGRAM_BOT_TOKEN="${TELEGRAM_BOT_TOKEN}"

COPY --chown=bot:bot bbsptbot.py .

VOLUME ["/bbsptbot/logs", "/bbsptbot/files"]

EXPOSE 3000

CMD ["python3", "bbsptbot.py"]